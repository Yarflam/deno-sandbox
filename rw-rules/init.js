/* [READ] Test: READ SUCCESS */
const test = Deno.readTextFileSync('./read/test.txt');
console.log(test);

/* [READ] Password: PERMISSION DENIED */
try {
    const passwd = Deno.readTextFileSync('./notouch/passwd.txt');
    console.log(passwd);
} catch (e) {
    console.error(e);
}

/* [WRITE] Coucou: WRITE SUCCESS */
Deno.writeTextFileSync('./write/coucou.txt', `coucou_${new Date().getTime()}`);

/* [WRITE] Lolilol: PERMISSION DENIED */
try {
    Deno.writeTextFileSync(
        './read/lolilol.txt',
        `lolilol_${new Date().getTime()}`
    );
} catch (e) {
    console.error(e);
}

/* [READ] Coucou: PERMISSION DENIED */
try {
    const coucou = Deno.readTextFileSync('./write/coucou.txt');
    console.log(coucou);
} catch (e) {
    console.error(e);
}
